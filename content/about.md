+++
title = "About"
description = "Hugo, the world's fastest framework for building websites"
date = "2021-07-17"
aliases = ["about-us", "about-hugo", "contact"]
author = "Ward Segers"
+++

Waste Manager (wasteman) is a CLI tool to manage trash and recycle folders.

It was written for an unfortunate event where a harddrive broke down. All we had, was the .Trash folder of another drive to recover the data on it. Unfortunately, no tool seemed to exist to make the folder more browseable. Therefore, we wrote wasteman.

The initial author and current maintainer is [Ward Segers](https://wardsegers.be).

The recycle infrastructure is clearly inspired by the #recycle folder that I found on my Synology NAS (I'm not sure how other vendors handle it). There, it periodically scans the folder for files that might be older than a threshold and then deletes them.

As it is written in Rust, it doesn't require a garbage collector (yes we thought that was funny).
